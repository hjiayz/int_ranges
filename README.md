# `int_ranges`

simple ranges tools for integer

```rust
    use int_ranges::{wrap,Ranges};
    let arr = vec![(1, 10), (30, 100), (11, 20)];
    let ranges: Vec<(i32, i32)> = Ranges::from(arr.clone()).into();

    assert_eq!(ranges, [(1, 20), (30, 100)]);

    let ranges: Vec<(i32, i32)> = Ranges::from(arr.clone()).invert().into();
    assert_eq!(ranges, [(i32::MIN, 0), (21, 29), (101, i32::MAX)]);

    assert_eq!(Ranges::<i32>::empty(), Ranges::<i32>::full().invert());
    assert_eq!(Ranges::<i32>::empty().invert(), Ranges::<i32>::full());
    assert_eq!(
        Ranges::from(vec![(1i32, 10)]).union(vec![(11i32, 20)]),
        Ranges::from(vec![wrap(1i32..=20)])
    );
    assert_eq!(
        Ranges::from(vec![(1i32, 10)]).intersect(vec![(11i32, 20)]),
        Ranges::empty()
    );

    assert_eq!(
        Ranges::from(vec![(1i32, 11)]).intersect(vec![(11i32, 20)]),
        Ranges::from(vec![(11i32,11)])
    );

    assert_eq!(
        Ranges::from(vec![(1i32, 15)]).intersect(vec![(11i32, 20)]),
        Ranges::from(vec![(11i32,15)])
    );

    assert_eq!(
        Ranges::from(vec![(1i32, 9)]).intersect(vec![(11i32, 20)]),
        Ranges::empty()
    );

    assert_eq!(
        Ranges::from(vec![(1i32, 10)]).contains(vec![(11i32, 20)]),
        false
    );

    assert_eq!(
        Ranges::from(vec![(1i32, 11)]).contains(vec![(11i32, 20)]),
        true
    );

```